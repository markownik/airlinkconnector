﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Net.Wifi;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Text.Format;
using Android.Views;
using Android.Widget;
using Tmds.MDns;
using Xamarin.Essentials;
using Debug = System.Diagnostics.Debug;

namespace AirLinkConnector
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        private readonly string _serviceType = "_oculusal._tcp";
        private string _myIp = string.Empty;
        private ServiceBrowser _serviceBrowser;
        private List<AirlinkServer> _services = new List<AirlinkServer>();
        private LinearLayout List => FindViewById<LinearLayout>(Resource.Id.computer_list);

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            _serviceBrowser = new ServiceBrowser();
            _serviceBrowser.ServiceAdded += OnServiceAdded;
            _serviceBrowser.ServiceRemoved += OnServiceRemoved;
            _serviceBrowser.ServiceChanged += OnServiceChanged;
        }

        protected override void OnResume()
        {
            base.OnResume();
            _services.Clear();
            WifiManager wm = (WifiManager)GetSystemService(Service.WifiService);
            _myIp = Formatter.FormatIpAddress(wm.ConnectionInfo.IpAddress).Trim();
            Debug.WriteLine($"my ip: {_myIp}");
            _serviceBrowser.StartBrowse(_serviceType);
        }

        protected override void OnPause()
        {
            base.OnPause();
            _serviceBrowser.StopBrowse();
        }

        private void OnServiceChanged(object sender, ServiceAnnouncementEventArgs e)
        {
            var announcement = e.Announcement;

            if (_services.Any(s => s.Name == announcement.Hostname))
            {
                _services.Clear();

                foreach (var address in announcement.Addresses)
                {
                    if (IpInSameNetwork(_myIp, address.ToString()))
                        _services.Add(new AirlinkServer()
                        {
                            Name = announcement.Hostname,
                            Ip = address.ToString(),
                            Port = announcement.Port.ToString()
                        });
                }
            }

            RefreshServices();
        }

        private void OnServiceRemoved(object sender, ServiceAnnouncementEventArgs e)
        {
            var announcement = e.Announcement;

            if (_services.Any(s => s.Name == announcement.Hostname))
            {
                _services.Remove(_services.Single(s => s.Name == announcement.Hostname));
            }

            RefreshServices();
        }

        private void OnServiceAdded(object sender, ServiceAnnouncementEventArgs e)
        {
            _services.Clear();
            var announcement = e.Announcement;

            foreach (var address in announcement.Addresses)
            {
                if (IpInSameNetwork(_myIp, address.ToString()))
                    _services.Add(new AirlinkServer()
                    {
                        Name = announcement.Hostname,
                        Ip = address.ToString(),
                        Port = announcement.Port.ToString()
                    });
            }

            RefreshServices();
        }

        private void RefreshServices()
        {
            MainThread.BeginInvokeOnMainThread(() =>
            {
                List.RemoveAllViews();

                foreach (var server in _services)
                {
                    Debug.WriteLine($"({server.Name}) {server.Ip}:{server.Port}");

                    Button btn = new Button(this);
                    btn.LayoutParameters = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);
                    btn.Text = server.Name;
                    btn.Enabled = true;
                    btn.Click += delegate(object sender, EventArgs args) { ConnectToAirLink(server); };

                    List.AddView(btn);
                }
            });
        }

        private void ConnectToAirLink(AirlinkServer server)
        {
            MainThread.BeginInvokeOnMainThread(() =>
            {
                var connString = $"xrstreamingclient://{server.Ip}:{server.Port}";
                Debug.WriteLine($"connecting to: \"{connString}\"");

                var intent = new Intent(Intent.ActionView);
                intent.SetData(Android.Net.Uri.Parse(connString));
                intent.SetPackage("com.oculus.xrstreamingclient");
                intent.AddFlags(ActivityFlags.NewTask);
                Application.StartActivity(intent);
            });
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        private static bool IpInSameNetwork(string me, string other)
        {
            if (string.IsNullOrWhiteSpace(me) || string.IsNullOrWhiteSpace(other))
                return false;

            return other.StartsWith(string.Join('.', me.Split('.').Take(3)));
        }
	}
}
