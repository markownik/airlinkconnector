﻿namespace AirLinkConnector
{
    public class AirlinkServer
    {
        public string Name { get; set; }

        public string Ip { get; set; }

        public string Port { get; set; }
    }
}